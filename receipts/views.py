from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, ExpenseCategoryForm, CreateAccount


# Create receipts list view
@login_required(login_url="/accounts/login/")
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


# Create create receipt form view
@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = (
                request.user
            )  # Adds the logged in user as purchaser to receipt implicitly
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceipt()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


# Create category list view
@login_required(login_url="/accounts/login/")
def categories_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/categories.html", context)


# Create account list view
@login_required(login_url="/accounts/login/")
def accounts_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/accounts.html", context)


# Create a create category form view
@login_required(login_url="/accounts/login/")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = (
                request.user
            )  # Adds the logged in user as owner to category implicitly
            category.save()
            return redirect("categories_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/categories/create.html", context)


# Create create account form view
@login_required(login_url="/accounts/login/")
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = (
                request.user
            )  # Adds the logged in user as owner to account implicitly
            account.save()
            return redirect("accounts_list")
    else:
        form = CreateAccount()
    context = {"form": form}
    return render(request, "receipts/accounts/create.html", context)
