from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


# Create receipt form
class CreateReceipt(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


# Create category form
class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


# create create account form
class CreateAccount(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
